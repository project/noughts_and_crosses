Module: Noughts and Crosses.

Project Page
=============
https://www.drupal.org/project/noughts_and_crosses


Description
============
This is basically fun game module.

In this Noughts and Crosses module, two players can play
Noughts and Crosses (Tic Tac Toe) game interactively.

The player who succeeds in placing three of their marks 
in a horizontal, vertical, or diagonal row wins the game.

Default standard permissions are set and handled.

Complete module is based on jQuery. However, non-js 
feature is also available and you can enable the facility by enabling
the submit button.


Requirements
============
No specific Module requirement.


Installation
============
1. Copy the module directory in to your Drupal 'modules' directory and 
install it as usual.

2. No need to Set / Modify the permissions separately.

3. Simple block for Game is provided for easier integration.

That's it! You are all set to use the module.


Usage
=====
1. Use Game Block as per your choice.


Configuration Settings
======================

